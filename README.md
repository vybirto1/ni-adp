# NI-ADP

This project is using [cpp_starter_project](https://github.com/lefticus/cpp_starter_project) as basic project setup and [find SDL2 scripts](https://github.com/brendan-w/collector) to find SDL2 packages. For text rendering I am using [Ubuntu font family](https://design.ubuntu.com/font/).

## Dependencies

Required:

* C++ compiler - GCC recomended
* [SDL2](https://www.libsdl.org/download-2.0.php) - rendering and input handeling
* [CMake](https://cmake.org/) - for project compilation
* [Conan](https://conan.io/) - for downloading extra libs

Extra:

* Doxygen (for documentation) - not neededd for game compilation
* libasan (for memory debugging) - must be turned on in `cmake/Sanitizers.cmake`
* cppcheck - must be turned on in `cmake/StaticAnalyzers.cmake`

## Controlls

Moving shooter:
* `W` - move up
* `S` - move down

Changing shooting angle:
* `A` - move up (increase angle)
* `D` - move down (decrease angle)

Changing shooting force:
* `E` - increase shooting force
* `Q` - decrease force

Shooting controlls:
* `Space` - shoot
* `1` - switch to single projectile shooting mode
* `2` - switch to multiple projectile shooting mode

Other:
* `R` - reset last saved state
* `F1` - enable debug mode
* `F2` - save current state (must be in debug mode)

## Compilation

First you need to generate Makefile using cmake.

```
mkdir build
cd build
cmake ..
```

### Game

To compile:

```
make shooter
```

To run:

```
cd bin
./shooter
```

Please run the game from bin directory.

### Documentation

To compile:

```
make doxygen-docs
```

Documentation is then generated into `build/html` folder.

Please reed documentation to make sure all cryteria were fullfilled.

### Tests

To compile tests:

```
make tests
```

To run tests:

```
cd bin
./tests
```