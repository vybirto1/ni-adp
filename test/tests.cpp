#include <catch2/catch.hpp>
#include <string>

#include "state/state.h"
#include "state/shooter.h"
#include "state/level.h"
#include "game/game.h"

TEST_CASE("Testing state history storing", "[state]")
{
    state cs;
    REQUIRE(cs.score == 0);
    REQUIRE(cs.player.position.get() == 240);

    cs.score = 100;
    cs.push();

    REQUIRE(cs.score == 100);

    cs.player.position.set(120);
    REQUIRE(cs.player.position.get() == 120);
    cs.push();

    cs.player.position.set(0);
    REQUIRE(cs.player.position.get() == 0);

    cs = state::pop();
    
    REQUIRE(cs.player.position.get() == 120);
    REQUIRE(cs.score == 100);

    cs = state::pop();

    REQUIRE(cs.player.position.get() == 240);
    REQUIRE(cs.score == 100);

    try {
        cs = state::pop();
        REQUIRE(false);
    } catch (const std::exception&) { }
}

TEST_CASE("Testing ranged_value structure", "[ranged_value]") {
    ranged_value<int> val(5, 0, 10);

    REQUIRE(val.get() == 5);
    
    val.set(3);

    REQUIRE(val.get() == 3);

    val.set(-5);

    REQUIRE(val.get() == 0);

    val.set(15);

    REQUIRE(val.get() == 10);
}

TEST_CASE("Testing game initialization", "[game]") {
    try {
        game g(30);
        REQUIRE(true);
    } catch (const std::exception& e) {
        std::string exception_string = e.what(); 
        REQUIRE(exception_string == "");
    }
}

TEST_CASE("Testing level loading", "[level]") {
    try {
        level::create().from_file("levels/01.txt");
        level l1 = level::get("start");
        REQUIRE(true);
    } catch (const std::exception& e) {
        std::string exception_string = e.what(); 
        REQUIRE(exception_string == "");
    }

    try {
        level l2 = level::get("gkfhdfgkdfghj");
        REQUIRE(false);
    } catch (const std::exception&) {
        REQUIRE(true);
    }
}

TEST_CASE("Testing shooting controllers", "[shoot_controller]") {
    shoot_controller sc = shooter_factory::get_single_shooter();

    REQUIRE(sc->shoot(false) == false);
    REQUIRE(sc->can_switch() == true);
    REQUIRE(sc->shoot(true) == true);
    REQUIRE(sc->can_switch() == false);
    REQUIRE(sc->shoot(true) == false);

    sc->update_timer(0.25);
    REQUIRE(sc->can_switch() == false);
    sc->update_timer(0.3);
    REQUIRE(sc->can_switch() == true);

    sc = shooter_factory::get_multi_shooter();
    REQUIRE(sc->shoot(false) == false);
    REQUIRE(sc->can_switch() == true);
    REQUIRE(sc->shoot(true) == true);
    REQUIRE(sc->can_switch() == false);
    REQUIRE(sc->shoot(true) == false);

    sc->update_timer(0.2);

    REQUIRE(sc->can_switch() == false);
    REQUIRE(sc->shoot(false) == true);
    REQUIRE(sc->can_switch() == false);

    sc->update_timer(0.1);

    REQUIRE(sc->shoot(false) == false);
    REQUIRE(sc->shoot(true) == false);
    REQUIRE(sc->can_switch() == false);

    sc->update_timer(0.1);

    REQUIRE(sc->shoot(true) == true);
    REQUIRE(sc->can_switch() == false);

    sc->update_timer(0.2);
    REQUIRE(sc->can_switch() == true);
}