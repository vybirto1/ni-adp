#include "abstract_input.h"
#include <stdexcept>

bool abstract_input::has_events() const {
    return index < events.size();
}

event& abstract_input::get_next_event() {
    if (events.size() > 0 && index < events.size()) {
        return events[index++];
    } else if (index >= events.size()) {
        throw std::runtime_error("All input events were processed");
    } else {
        throw std::runtime_error("No events polled.");
    }
}