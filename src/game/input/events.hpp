#ifndef EVENTS_HPP
#define EVENTS_HPP

#include <variant>
#include <cstdint>

/// Event that represents data change
template<typename T>
struct change {
    T data;
};

/// Move vector data that are applied during changing/setting values
struct move_vector {
    constexpr static int32_t DEFAULT = 5;
    int32_t value;
};

/// Player angle data that are applied during changing/setting values
struct player_angle {
    constexpr static double DEFAULT = 0.05;
    double value;
};


/// Player force data that are applied during changing/setting values
struct player_force {
    constexpr static double DEFAULT = 0.1;
    double value;
};

/// All posible shooting modes
enum class shooting_mode {
    SINGLE,
    MULTIPLE
};

/// Shooting controller data that are applied during changing/setting values
struct shooting_controller {
    shooting_mode mode;
};

/// Event that pushes current state
struct push_state {};

/// Pops latest state and replaces it
struct pop_state {};

/// Quit game event - stops game loop
struct quit_game {};

/// Shoot event that represents user pushing shooting button
struct shoot_event {};

/// Changes debug status
struct switch_debug {};

/// Generic all event data storage - saves memory, autotype deduction
typedef std::variant<change<player_force>,
                     change<move_vector>,
                     change<player_angle>,
                     change<shooting_controller>,
                     push_state, pop_state,
                     quit_game, shoot_event,
                     switch_debug> event;

#endif//EVENTS_HPP