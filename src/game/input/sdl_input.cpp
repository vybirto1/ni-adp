#include "sdl_input.h"
#include "events.hpp"

#include <stdexcept>

void sdl_input::poll_events() {
    index = 0;
    SDL_Event e;
    events.clear();
    while (SDL_PollEvent(&e)) {
        switch(e.type) {
            case SDL_KEYUP:
            case SDL_KEYDOWN:
                process_key_event(e);
            break;
            case SDL_QUIT:
                events.emplace_back(quit_game{});
            default: break;
        }
    }
}

void sdl_input::process_key_event(const SDL_Event& e) {
    bool pressed = e.key.state == SDL_PRESSED;
    switch (e.key.keysym.sym) {
        case SDLK_w:
            if (pressed)
                events.emplace_back(change<move_vector>{-move_vector::DEFAULT});
            else
                events.emplace_back(change<move_vector>{move_vector::DEFAULT});
        break;
        case SDLK_s:
            if (pressed)
                events.emplace_back(change<move_vector>{move_vector::DEFAULT});
            else
                events.emplace_back(change<move_vector>{-move_vector::DEFAULT});
        break;
        case SDLK_a:
            if (pressed)
                events.emplace_back(change<player_angle>{player_angle::DEFAULT});
            else
                events.emplace_back(change<player_angle>{-player_angle::DEFAULT});
        break;
        case SDLK_d:
            if (pressed)
                events.emplace_back(change<player_angle>{-player_angle::DEFAULT});
            else
                events.emplace_back(change<player_angle>{player_angle::DEFAULT});
        break;
        case SDLK_e:
            if (pressed)
                events.emplace_back(change<player_force>{player_force::DEFAULT});
            else
                events.emplace_back(change<player_force>{-player_force::DEFAULT});
        break;
        case SDLK_q:
            if (pressed)
                events.emplace_back(change<player_force>{-player_force::DEFAULT});
            else
                events.emplace_back(change<player_force>{player_force::DEFAULT});
        break;
        case SDLK_1:
            if (pressed)
                events.emplace_back(change<shooting_controller>{shooting_mode::SINGLE});
        break;
        case SDLK_2:
            if (pressed)
                events.emplace_back(change<shooting_controller>{shooting_mode::MULTIPLE});
        break;
        case SDLK_SPACE:
            if (pressed)
                events.emplace_back(shoot_event{});
        break;
        case SDLK_r:
            if (pressed)
                events.emplace_back(pop_state{});
        break;
        case SDLK_F1:
            if (pressed)
                events.emplace_back(switch_debug{});
        break;
        case SDLK_F2:
            if (pressed)
                events.emplace_back(push_state{});
        break;
        case SDLK_ESCAPE:
            events.emplace_back(quit_game{});
        break;
        default: break;
    }
}

sdl_input::sdl_input():abstract_input() {
    if (SDL_InitSubSystem(SDL_INIT_EVENTS) != 0) {
        throw std::runtime_error("Couldn't initialize event system");
    }
}

sdl_input::~sdl_input() {
    SDL_QuitSubSystem(SDL_INIT_EVENTS);
}