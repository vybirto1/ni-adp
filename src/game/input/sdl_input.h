#ifndef SDL_INPUT_H
#define SDL_INPUT_H

#include "abstract_input.h"
#include <SDL2/SDL.h>

/// Processes user inputs using SDL's own system.
class sdl_input: public abstract_input {
public:
    /// Initializes SDL input system
    sdl_input();
    /// Deinitializes SDL input system
    ~sdl_input();
    /// Polls events using SDL_PollEvent funtion.
    void poll_events() override;
protected:
    /// Processes all key events.
    void process_key_event(const SDL_Event& e);
};

#endif//SDL_INPUT_H