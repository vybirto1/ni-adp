#ifndef ABSTRACT_INPUT_H
#define ABSTRACT_INPUT_H

#include <vector>
#include "events.hpp"

/// Abstract class that defines common interface for all input processors
class abstract_input {
public:
    /// Virtual destructor
    virtual ~abstract_input() = default;

    /// Check if there are any events to be proccessed
    bool has_events() const;
    /// Tryies to get next evet. Can throw exception if there is no event to be pulled
    event& get_next_event();
    /// virtual function to poll events from underlying api
    virtual void poll_events() = 0;
protected:
    size_t index; ///< Current index of polled event
    std::vector<event> events; ///< Vector of polled events;
};

#endif//ABSTRACT_INPUT_H