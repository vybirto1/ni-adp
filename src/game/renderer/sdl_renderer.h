#ifndef SDL_RENDERER_H
#define SDL_RENDERER_H

#include <SDL2/SDL.h>

#include "abstract_renderer.h"
#include "sdl_resources.h"

/// Game renderer class. Using SDL2 to render game
class sdl_renderer: public abstract_renderer {
public:
    /// Default contructor (initializes SDL, window, renderer and loads texture into memory)
    sdl_renderer();
    /// Closes window and deintializes sdl video module
    virtual ~sdl_renderer();

    /// Renders passed state to created window
    void render_state(const state& current_state, bool debug_ui) override;
    /// Renders win dialog uisng SDL api
    void show_win_dialog() override;
protected:
    /// Helper function that renders all enemies to screen.
    void render_enemies(const std::vector<enemy>& enemies);
    /// Helper function that renders all projectiles to screen.
    void render_projectiles(const std::vector<projectile>& enemies);
    /// Helper function that renders debug ui to screen.
    void render_ui(const state& cs, bool debug);
    /// Helper function that renders texture to the screen.
    void render_texture(SDL_Texture* texture, const vector2& pos, const vector2& size);
    /// Helper function that renders text
    void render_text(const std::string& text, const vector2& pos);
    /// Helper function that renders all the hits
    void render_hits(const std::vector<vector2>& hits);

    /// SDL window struct pointer
    SDL_Window* window;
    /// SDL renderer struct pointer
    SDL_Renderer* renderer;
    /// Collection of resources used to render.
    sdl_resources* resources;
};

#endif//SDL_RENDERER_H