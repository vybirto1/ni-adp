#ifndef SDL_RESOURCES_H
#define SDL_RESOURCES_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <string>

/// Resource class that load all the resources for game rendering.
class sdl_resources {
public:
    /// Loads resources
    explicit sdl_resources(SDL_Renderer* curr_renderer);
    /// Unloads resources
    ~sdl_resources();

    /// Shooter texture
    SDL_Texture* shooter;
    /// Projectile texture
    SDL_Texture* projectile;
    /// Enemy 1 texture
    SDL_Texture* enemy1;
    /// Enemy 2 texture
    SDL_Texture* enemy2;
    /// Enemy 3 texture
    SDL_Texture* enemy3;
    /// Hit image texture
    SDL_Texture* hit;
    /// Main font to render text with
    TTF_Font* main_font;
protected:
    SDL_Renderer* renderer;
    /// Loads texture by path defined by path
    SDL_Texture* load_texture(const std::string& path_to_image);
};

#endif//SDL_RESOURCES_H