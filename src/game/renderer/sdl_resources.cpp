#include "sdl_resources.h"

#include <SDL2/SDL_image.h>

#include <stdexcept>

sdl_resources::sdl_resources(SDL_Renderer* curr_renderer): renderer(curr_renderer) {
    if (! (IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)) {
        throw std::runtime_error(IMG_GetError());
    }
    shooter = load_texture("images/cannon.png");
    enemy1 = load_texture("images/enemy1.png");
    enemy2 = load_texture("images/enemy2.png");
    enemy3 = load_texture("images/enemy3.png");
    projectile = load_texture("images/missile.png");
    hit = load_texture("images/kaboom.png");

    if ( TTF_Init() < 0 ) {
        throw std::runtime_error(TTF_GetError());
    }
    main_font = TTF_OpenFont("Ubuntu-C.ttf", 16);
}

sdl_resources::~sdl_resources() {
    TTF_CloseFont(main_font);
    TTF_Quit();

    SDL_DestroyTexture(shooter);
    SDL_DestroyTexture(projectile);
    SDL_DestroyTexture(enemy1);
    SDL_DestroyTexture(enemy2);
    SDL_DestroyTexture(enemy3);
    SDL_DestroyTexture(hit);
    IMG_Quit();
}

SDL_Texture* sdl_resources::load_texture(const std::string& path_to_image) {
    SDL_Surface * surface = nullptr;
    SDL_Texture* texture = nullptr;
    surface = IMG_Load(path_to_image.c_str());
    if (surface == nullptr) {
        throw std::runtime_error(IMG_GetError());
    }
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    if (texture == nullptr) {
        throw std::runtime_error(SDL_GetError());
    }
    return texture;
}