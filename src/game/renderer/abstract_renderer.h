#ifndef ABSTRACT_RENDERER_H
#define ABSTRACT_RENDERER_H

#include "../state/state.h"

/// Abstract class that represents generic renderer template that can be used by any framework.
class abstract_renderer{
public:
    /// Virtual destructor to prevent memory leaks
    virtual ~abstract_renderer() = default;
    /// Virtual draw method that is called by the game controller.
    virtual void render_state(const state& current_state, bool debug_ui) = 0;
    /// Shows game win dialog
    virtual void show_win_dialog() = 0;
};

#endif//ABSTRACT_RENDERER_H