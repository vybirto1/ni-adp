#include "sdl_renderer.h"

#include <SDL2/SDL.h>

#include <stdexcept>
#include <sstream>
#include <cmath>

sdl_renderer::sdl_renderer(): window(nullptr), renderer(nullptr), resources(nullptr) {
    if (SDL_InitSubSystem(SDL_INIT_VIDEO) != 0) {
        throw std::runtime_error(SDL_GetError());
    }
    window = SDL_CreateWindow("Window NAME - Replace me",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              640, 480, 0);
    if (window == nullptr) {
        throw std::runtime_error(SDL_GetError());
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr) {
        throw std::runtime_error(SDL_GetError());
    }

    resources = new sdl_resources(renderer);

    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderPresent(renderer);
}

sdl_renderer::~sdl_renderer() {
    if (renderer != nullptr)
        SDL_DestroyRenderer(renderer);
    if (window != nullptr)
        SDL_DestroyWindow(window);
    if (resources != nullptr)
        delete resources;
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

void sdl_renderer::render_state(const state& cs, bool debug_ui) {
    SDL_RenderClear(renderer);
    render_texture(resources->shooter, {0, cs.player.position.get() - 14}, {25, 69});
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderDrawLine(renderer, 13, cs.player.position.get(),
                       13 + static_cast<int>(16*cs.player.force.get()*cos(cs.player.angle.get())),
                       cs.player.position.get() - static_cast<int>(16*cs.player.force.get()*sin(cs.player.angle.get())));
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

    render_enemies(cs.enemies);
    render_projectiles(cs.projectiles);
    render_hits(cs.hits);
    render_ui(cs, debug_ui);
    SDL_RenderPresent(renderer);
}

void sdl_renderer::render_texture(SDL_Texture* texture, const vector2& pos, const vector2& size) {
    SDL_Rect position;
    position.x = pos.x;
    position.y = pos.y;
    position.w = size.x;
    position.h = size.y;
    SDL_RenderCopy(renderer, texture, nullptr, &position);
}

void sdl_renderer::render_text(const std::string& text, const vector2& pos) {
    static SDL_Color text_color{0, 0, 0, 255};
    SDL_Rect text_pos;
    text_pos.x = pos.x;
    text_pos.y = pos.y;

    SDL_Surface* text_surface = TTF_RenderText_Solid(resources->main_font, text.c_str(), text_color);
    text_pos.w = text_surface->w;
    text_pos.h = text_surface->h;

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);

    SDL_RenderCopy(renderer, text_texture, nullptr, &text_pos);

    SDL_DestroyTexture(text_texture);
    SDL_FreeSurface(text_surface);
}

void sdl_renderer::render_enemies(const std::vector<enemy>& enemies) {
    static vector2 enemy_pic_size{32, 32};
    for (auto& en: enemies) {
        vector2 pos{en.position.x - static_cast<int>(en.radius),
                    en.position.y - static_cast<int>(en.radius)};
        switch(en.variant) {
            case 0:
                render_texture(resources->enemy1, pos, enemy_pic_size);
            break;
            case 1:
                render_texture(resources->enemy2, pos, enemy_pic_size);
            break;
            default:
                render_texture(resources->enemy3, pos, enemy_pic_size);
            break;
        }
    }
}

void sdl_renderer::render_projectiles(const std::vector<projectile>& projectiles) {
    static vector2 projectile_size{30, 30};
    for (auto& proj: projectiles) {
        vector2 pos{proj.position.x - static_cast<int>(proj.radius),
                    proj.position.y - static_cast<int>(proj.radius)};
        render_texture(resources->projectile, pos, projectile_size);
    }
}

void sdl_renderer::render_ui(const state& cs, bool debug) {
    SDL_Rect bar{0, 0, 640, 20};
    SDL_SetRenderDrawColor(renderer, 200, 200, 255, 255);
    SDL_RenderFillRect(renderer, &bar);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    std::ostringstream oss;
    if (debug) {
        oss << "P: " << cs.player.position.get() << ";A: " << cs.player.angle.get()*180/M_PI;
        oss << ";F: " << cs.player.force.get();
        oss << ";PC: " << cs.projectiles.size() << ";EC: " << cs.enemies.size() << ";";
    }
    oss << "SCORE: " << cs.score;
    render_text(oss.str(), {2, 0});
}

void sdl_renderer::show_win_dialog() {
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Victory",
        "Congratulation you have WON the game.", window) != 0) {
        throw std::runtime_error(SDL_GetError());
    }
}

void sdl_renderer::render_hits(const std::vector<vector2>& hits) {
    vector2 size{64, 64};
    for (auto hit: hits) {
        vector2 pos {hit.x - 32, hit.y - 32};
        render_texture(resources->hit, pos, size);
    }
}