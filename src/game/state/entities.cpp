#include "entities.h"

#include <random>
#include <chrono>

#include <cmath>

entity::entity(const vector2& initial_position, uint32_t rad):position(initial_position), radius(rad) { }

bool entity::did_collide(const entity& other) const {
    uint32_t min_distance = radius + other.radius;
    double actual_distance = sqrt(pow(position.x - other.position.x, 2) + 
                                  pow(position.y - other.position.y, 2));
    return actual_distance <= min_distance;
}

enemy::enemy(const vector2& initial_position, uint32_t rad): entity(initial_position, rad), variant(0) {
    std::mt19937 rng(
        static_cast<uint32_t>(
            std::chrono::high_resolution_clock::now().time_since_epoch().count()));
    variant = static_cast<uint32_t>(rng()) % MAX_VARIATIONS;
}

projectile::projectile(const vector2& initial_position, uint32_t rad, uint32_t initial_speed, double initial_angle):
                       entity(initial_position, rad), speed(initial_speed), angle(initial_angle) { }
