#ifndef SHOOTER_H
#define SHOOTER_H

#include <cstdint>
#include <cmath>

/// Templated min function
template<typename T>
T min(const T& a, const T& b) {
    return (a > b)?b:a;
}

/// Templated max function
template<typename T>
T max(const T& a, const T& b) {
    return (a > b)?a:b;
}


/// @brief Helper class that helps to keep value in specified range.
/// Since it is templated it needs to be defined in header. (Actually it might not be if I not use it some other library)
template<typename T>
class ranged_value {
public:
    /// Contructs the class and defines starting value, minimal and maximal value
    ranged_value(T start, T min, T max): val(start), val_min(min), val_max(max) {}

    /// Change current value and limits its value to predefined range
    void set(T new_val) {
        val = max(val_min, min(val_max, new_val));
    }

    /// Return copy of current value
    T get() const {
        return val;
    }

    /// Return reference of current value
    T& get() {
        return val;
    }
protected:
    /// Current value
    T val;
    /// Minimal value
    const T val_min;
    /// Maximal value
    const T val_max;
};


/// State class containing values about shooter
class shooter {
public:
    /// Default contructor (defines ranges for each value)
    shooter();
    /// Default copy constructor
    shooter(const shooter& other) = default;
    /// Assign copy operator
    shooter& operator=(const shooter& other);

    /// Current shooter position
    ranged_value<int32_t> position;
    /// Maximal value of position
    constexpr static int32_t MAX_POSITION = 480;
    /// Current shooter angle - value is limited to 0 - pi/2
    ranged_value<double> angle;
    /// Maximal value of angle
    constexpr static double MAX_ANGLE = M_PI/2.0;
    /// Current force value
    ranged_value<double> force;
    /// Max value of force
    constexpr static double MAX_FORCE = 2.0;
    /// Min value of force
    constexpr static double MIN_FORCE = 1.0;
};

#endif//SHOOTER_H