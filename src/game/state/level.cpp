#include "level.h"

#include <stdexcept>
#include <memory>
#include <fstream>

std::map<std::string, level> level::levels;

level::level(): enemies(), next_level_id() { }

level::level(const level& other): enemies(other.enemies),
                                  next_level_id(other.next_level_id) { }

level& level::operator=(const level& other) {
    enemies = other.enemies;
    next_level_id = other.next_level_id;
    return *this;
}

level_creator level::create() {
    return level_creator();
}

level level::get(const std::string& level_id) {
    return level::levels.at(level_id);
}

level_creator::level_creator(): to_be_created() { }

level_creator& level_creator::with_enemy_at(const vector2& position) {
    to_be_created.enemies.emplace_back(position, 16);
    return *this;
}

level_creator& level_creator::set_next_level(const std::string& level_id) {
    to_be_created.next_level_id = level_id;
    return *this;
}

void level_creator::save_as(const std::string& level_id) {
    level::levels[level_id] = to_be_created;
}

void level_creator::from_file(const std::string& path_to_file) {
    std::ifstream ifs(path_to_file);
    if (!ifs.good()) {
        throw std::runtime_error("Can't find level file at: " + path_to_file);
    }
    std::string tmp, id;
    ifs >> tmp;
    if (tmp != "LEVEL:" || !ifs.good()) {
        throw std::runtime_error("Can't load level info. LEVEL id must be first information");
    }
    ifs >> id;
    if (id.size() == 0 || !ifs.good()) {
        throw std::runtime_error("Can't load level info. No id data");
    }
    ifs >> tmp;
    if (tmp != "NEXT:" || !ifs.good()) {
        throw std::runtime_error("Can't load level info. No id data or second entry isn't next level id.");
    }
    ifs >> to_be_created.next_level_id;
    if (to_be_created.next_level_id.size() == 0 || !ifs.good()) {
        throw std::runtime_error("Can't load level info. No next level id data found.");
    }
    ifs >> tmp;
    if (tmp != "ENEMIES:" || !ifs.good()) {
        throw std::runtime_error("Can't load level info. No next level id data found or missing ENEMIES tag.");
    }
    while (true) {
        vector2 position;
        ifs >> position.x >> position.y;
        if (!ifs.good())
            break;
        to_be_created.enemies.emplace_back(position, 16);
    }
    if (to_be_created.enemies.size() == 0) {
        throw std::runtime_error("Can't load level info. No enemies data were suplied.");
    }
    level::levels[id] = to_be_created;
}

level level_creator::get() {
    return to_be_created;
}