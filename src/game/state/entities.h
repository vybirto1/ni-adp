#ifndef ENTITIES_H
#define ENTITIES_H

#include <cstdint>

/// Max of enemy texture variations
const uint32_t MAX_VARIATIONS = 3;

/// 2d vector of integers
struct vector2 {
    int32_t x, y;
};

/// Base entity class
class entity {
public:
    /// Creates base entity with nessessary data
    entity(const vector2& initial_position, uint32_t rad);
    /// Tests for collision with other entity
    bool did_collide(const entity& other) const;

    /// Current entity position
    vector2 position;
    /// Entity radius (to test in collisions)
    uint32_t radius;
};

/// Enemy data class (extends entity class)
class enemy : public entity {
public:
    /// Creates enemy data representation
    enemy(const vector2& initial_position, uint32_t rad);

    /// Variants of enemy that changes it looks
    uint32_t variant;
};


/// Projectile data class
class projectile: public entity {
public:
    /// Primary constructor
    projectile(const vector2& initial_position, uint32_t rad, uint32_t initial_speed, double initial_angle);

    /// Speed of projectile
    uint32_t speed;
    /// Angle of projectile
    double angle;
};

#endif//ENTITIES_H