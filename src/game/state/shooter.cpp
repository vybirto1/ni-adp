#include "shooter.h"

shooter::shooter():position{240, 0, shooter::MAX_POSITION},
                   angle{0.0, 0.0, shooter::MAX_ANGLE},
                   force{1.0, MIN_FORCE, MAX_FORCE} { }

shooter& shooter::operator=(const shooter& other) {
    position.set(other.position.get());
    angle.set(other.angle.get());
    force.set(other.force.get());
    return *this;
}