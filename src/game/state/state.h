#ifndef STATE_H
#define STATE_H

#include <list>
#include <vector>
#include <string>

#include "entities.h"
#include "shooter.h"



/// @brief This class stores games state. Also it can store history of all previous states.
/// This is what is serialized while saving/loading the game.
class state {
public:
    /// Default constructor
    state();
    /// Copy constructor to store states in history
    state(const state& other) = default;

    /// Copy operator to store state in history
    state& operator=(const state& other);

    /// Pushes current state to history
    void push() const;
    /// Retrurns latest state in history and pops it from stack
    static state pop();
    
    /// Player shooter data representation
    shooter player;
    /// All alive enemies
    std::vector<enemy> enemies;
    /// All visible projectiles
    std::vector<projectile> projectiles;
    /// Current game score
    uint32_t score;
    /// Id of next level to load to this state
    std::string next_level_id;
    /// Check if there are enough stored states
    std::vector<vector2> hits;
    static bool can_pop();
protected:
    /// List of all saved states.
    static std::list<state> history;
};

#endif//STATE_H