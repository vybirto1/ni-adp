#ifndef LEVEL_H
#define LEVEL_H

#include <map>
#include <string>
#include <vector>

#include "entities.h"

class level_creator;

/// Level data structure. Contains list of enemies to spawn and next level id.
class level {
public:
    /// Default contructor - creates empty level with empty next level id.
    level();
    /// Copy constructor - to be able to pass level from funtions
    level(const level& other);
    /// Copy operator
    level& operator=(const level& other);

    /// List of all enemies that are going to be spawned in this level.
    std::vector<enemy> enemies;
    /// Next level id. If empty than games should end.
    std::string next_level_id;

    /// Static map of all levels in level_id -> level relation.
    static std::map<std::string, level> levels;

    /// Creates level_creator class that can create/load level from scratch
    static level_creator create();
    /// Returns level by defined level id. Throws out of range error if level with defined id doesn't exist.
    static level get(const std::string& level_id);
};

/// Class that is able to create/load level data.
class level_creator {
public:
    /// Creates basic level_creator
    level_creator();
    /// Copy contructor
    level_creator(const level_creator& other) = default;

    /// Adds enemy at defined position
    [[nodiscard]] level_creator& with_enemy_at(const vector2& position);
    /// Set next level id to current level
    [[nodiscard]] level_creator& set_next_level(const std::string& level_id);
    /// Saves level with defined id.
    void save_as(const std::string& level_id);
     /// Loads level from file.
     /// If file doesn't exists it throws an error.
     /// File must be in this format:
     /// LEVEL:
     ///     {level_id}
     /// NEXT:
     ///     {next_level_id}
     /// ENEMIES:
     ///     {list of positions as pairs of number seperated by white spaces}
    void from_file(const std::string& path_to_file);
    /// Returns currently created level
    [[nodiscard]] level get();
protected:
    /// Level that is being created
    level to_be_created;
};

#endif//LEVEL_H