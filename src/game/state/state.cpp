#include "state.h"

#include <stdexcept>
#include <cstring>

state::state():player(), enemies(), projectiles(), score(0), next_level_id("") { }

void state::push() const {
    history.emplace_back(*this);
}

state state::pop() {
    state top;
    if (!history.empty()) {
        top = history.back();
        history.pop_back();
    } else {
        throw std::runtime_error("History is empty. So nothing can be poped.");
    }
    return top;
}

state& state::operator=(const state& other) {
    player = other.player;
    enemies = other.enemies;
    projectiles = other.projectiles;
    score = other.score;
    next_level_id = other.next_level_id;
    return *this;
}

bool state::can_pop() {
    return state::history.size() > 0;
}

std::list<state> state::history;