#include "multi_shot.h"

multi_shot::multi_shot():shoot_controller_impl(DELAY), count(0) { }

bool multi_shot::shoot(bool did_shoot) {
    if (count == 0 && can_shoot() && did_shoot) {
        count = START_COUNT - 1;
        remaining_time = shoot_delay;
        return true;
    }
    if (count > 0 && can_shoot()) {
        count --;
        remaining_time = shoot_delay;
        return true;
    }
    return false;
}

bool multi_shot::can_switch() {
    return can_shoot() && count == 0;
}