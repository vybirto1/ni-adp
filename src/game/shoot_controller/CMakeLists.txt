add_library(shoot_controller
            shoot_controller.h shoot_controller.cpp
            multi_shot.h multi_shot.cpp
            single_shot.h single_shot.cpp)

target_include_directories(shoot_controller PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/..)

target_link_libraries(shoot_controller
    PRIVATE
    project_options
    project_warnings)