#ifndef SHOOT_CONTROLLER_H
#define SHOOT_CONTROLLER_H

#include <memory>

/// Controls shooting behavior
class shoot_controller_impl {
public:
    
    /// Initializes controller with max delay 
    explicit shoot_controller_impl(double delay);
    /// Virtual destructor to prevent memory leaks
    virtual ~shoot_controller_impl() = default;

    /// Try to shoot - returns true if can
    virtual bool shoot(bool did_shoot);
    /// Updates shooting delay timer
    void update_timer(double delta_time);
    /// Checks if the game can change shoot controller
    virtual bool can_switch();
protected:
    /// Checks if timer is expired
    bool can_shoot() const;

    /// Remaining time before you can shoot again
    double remaining_time;
    /// Dealy between two shots
    double shoot_delay;
};

/// Hiding shared pointer under other name
typedef std::shared_ptr<shoot_controller_impl> shoot_controller;

/// Easy to use static factory that creates new shooting modes
class shooter_factory {
public:
    /// Returns single shot controller
    static shoot_controller get_single_shooter();
    /// Returns multi shot controller
    static shoot_controller get_multi_shooter();
};

#endif//SHOOT_CONTROLLER_H