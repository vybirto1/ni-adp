#ifndef MULTI_SHOT_H
#define MULTI_SHOT_H

#include <cstdint>

#include "shoot_controller.h"

/// Controller that shoots multiple shot slightly delayed
class multi_shot: public shoot_controller_impl {
public:
    /// Contructs controller with default values
    multi_shot();
    /// Overrides shoot logic to accomodate for multiple projectiles
    bool shoot(bool did_shoot) override;
    /// Overrides can_switch check to accomodate for multiple projectiles
    bool can_switch() override;
    /// Default delay
    constexpr static double DELAY = 0.2;
    /// Count of projectiles
    constexpr static uint32_t START_COUNT = 3;
protected:
    /// Count of remaining shots
    uint32_t count;
};

#endif//MULTI_SHOT_H