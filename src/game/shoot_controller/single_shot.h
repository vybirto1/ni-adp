#ifndef SINGLE_SHOT_H
#define SINGLE_SHOT_H

#include "shoot_controller.h"

/// Controller that shoots simple shot
class single_shot: public shoot_controller_impl {
public:
    /// Default contructor that initializes controller with default values
    single_shot();
    /// Default delay
    constexpr static double DELAY = 0.5;
};

#endif//SINGLE_SHOT_H