#include "shoot_controller.h"
#include "single_shot.h"
#include "multi_shot.h"

shoot_controller_impl::shoot_controller_impl(double delay): remaining_time(0), shoot_delay(delay) { }

void shoot_controller_impl::update_timer(double delta_time) {
    remaining_time -= delta_time;
    if (remaining_time < 0.0) {
        remaining_time = 0.0;
    }
}

bool shoot_controller_impl::can_shoot() const {
    return remaining_time == 0.0;
}

bool shoot_controller_impl::shoot(bool did_shoot) {
    if (can_shoot() && did_shoot) {
        remaining_time = shoot_delay;
        return true;
    }
    return false;
}

bool shoot_controller_impl::can_switch() {
    return can_shoot();
}


shoot_controller shooter_factory::get_single_shooter() {
    return std::make_shared<single_shot>();
}

shoot_controller shooter_factory::get_multi_shooter() {
    return std::make_shared<multi_shot>();
}