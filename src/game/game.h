#ifndef GAME_H
#define GAME_H

#include <cstdint>
#include <functional>

#include "state/state.h"
#include "input/abstract_input.h"
#include "renderer/abstract_renderer.h"
#include "shoot_controller/shoot_controller.h"

/// Basic game structure - behaves like controler in MVC schema.
class game {
public:
    /// Initializes the game with default/intended input processors and renderer
    explicit game(uint32_t fps);
    /// Deletes all alocated resources.
    ~game();
    /// Runs basic game loop
    void run_loop();
    /// Loads all levels
    static void load_levels();
protected:
    /// Processes polled events
    void process_events();
    /// Processes singular event
    void process_event(const event& e);
    /// Updates state
    void update_state();
    /// Renders current state
    void render_current_state();
    /// Helper function that processes event of defined type using provided funtion (lambda) 
    template<typename T>
    void do_event(const event& event, const std::function<void(const T&)>& trigger);
    /// Tries to load next level. Returns true if can't load any new levels.
    bool try_to_switch_level();
    /// Moves projectiles and tests them for collisions
    void move_and_test_collisions();

    /// Current game state
    state current_state;
    /// Current input processor
    abstract_input* input_processor;
    /// Current game renderer
    abstract_renderer* renderer;
    /// Controlls current shooting mode
    shoot_controller shoot_control;
    /// Player position is updated every second by this value
    int32_t player_position_delta;
    /// Player angle is updated every second by this value
    double player_angle_delta;
    /// Player force is updated every second by this value
    double player_force_delta;
    /// Time between two ticks
    double delta_time;
    /// Is the game running
    bool is_running;
    /// If true controller tries to create new projectile
    bool shoot;
    /// Is in debug mode
    bool debug;
};

#endif//GAME_H