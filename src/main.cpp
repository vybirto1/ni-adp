#include <memory>
#include <string>
#include <stdexcept>
#include <iostream>

#include "game/game.h"

/**
 * \mainpage NI-ADP Documentation
 *
 * School project for subject NI-ADP (Advanced design patterns).
 * The assingment is to create simple game that uses at least 10 design patterns.
 * Recomended patterns are:
 *
 * * MVC
 *   * Whole game is in MVC pattern
 *   * Model - whole state lib should be considered model - contains all data.
 *   * Controller - game class is the main controller with 2 extra speciliazed controllers - input controller and shooting controller
 *   * View - whole renderer lib should be considered as view - renders all data.
 * * Strategy - Shooting mode switching
 * * Bridge - Game graphics rendering
 * * Proxy - Lots of uses - one example is level loading -> game controller calls level class (data representation) -> that creates level_creator class
 * * State - Both shooting controllers - player can't shoot again until some time passes.
 * * Visitor - Processing of custom events
 * * Observer - Technically event processing is using something like observer, but instead of observer I used Factory to create levels
 * * Command - SDL events are processed and turned into custom game events
 * * Memento - Game is able to remember it states and revert them
 * * Abstract factory - Class shooter_factory allows simplified access to both shooting modes
 *
 * force change - implemented
 *
 * angle of the cannon (angle) - implemented
 *
 * gravity - it's not "real" gravity but projectile falls down
 *
 * score counting - implemented
 *
 * reduction of dependency on a specific graphics library - implemented
 *
 * possibility to fire multiple shots at once - implemented
 * 
 * model control by commands - implemented
 * 
 * step-back - implemented
 *
 * 2 missile movement strategies - implemented
 *
 * at least 5 unit cases (test cases) in at least 2 test suites and Mocking - kinda implemented - Catch2 doesn't support suites, but you can split every test case into sections, so I technically created 5 test suites with 1 test case each. Mocking done for shooting controlls testing.
 *
 * For more info check @ref about_impl page.
 */


/**
 * @page about_impl Implementation
 * I would like to say some things about the implementation.
 * ## Why c++17?
 * Because in the begining c++20 wasn't completly finished (you can use some features but not all of them)
 * ## Why cmake?
 * Easier to manage project than simple Makefile. Also I wanted to created project using this project https://github.com/lefticus/cpp_starter_project.
 * ## Why conan?
 * I wanted to use more external libs, but in middle of development there were some updates and some of those libs didn't compile (download) anymore.
 * In the end it is used for testing (using Catch2)
 * ## Level loading/creation
 * Levels can be created in code with using level_factory class or can be loaded from file.
 * ## State saving
 * You can manually save state if you are in debug mode (`F1`) by pressing `F2`. But normally state is saved before shooting and if there are no projectiles on the screen (so you can revert before you shot that shot).
 * ## Debug mode
 * Allows for the player to display actual values of shooter angle, force and position.
 * ## Projectile deleting
 * If projectile moves out of screen it is deleted.
 * ## About nameing scheme
 * All headers only parts of code are in *.hpp files. If implementation is separate from 
 */

/// Main function that initializes and starts the game.
int main() {
    try {
        game game(30);
        game.run_loop();
    } catch (const std::exception& exp) {
        std::cerr << "Unhadled exception caught: " << exp.what() << std::endl;
        return 1;
    }
    return 0;
}